const inputForm = document.getElementById('input-form');
const csvInput = document.getElementById('csv-input');
const textOutput = document.getElementById('text-output');

// Need these for sorting outputs
const stockCars = ['rc', 'mite', 'phat', 'moss', 'mud', 'beatall', 'volken', 'tc6', 'dino', 'candy', 'gencar', 'tc4', 'mouse', 'flag', 'tc2', 'r5', 'tc5', 'sgt', 'tc3', 'adeon', 'fone', 'tc1', 'rotor', 'cougar', 'sugo', 'toyeca', 'amw', 'panga'];
const dreamcastCars = ['bigvolt', 'bossvolt', 'jg6rc', 'tc12', 'tc10', 'tc8', 'tc11', 'tc9', 'jg1jg7', 'tc7', 'jg3loco', 'jg4snw35', 'jg5purpxl', 'jg2fulonx'];


inputForm.addEventListener('submit', e => {
    e.preventDefault();
    const inputFile = csvInput.files[0];
    const reader = new FileReader();

    reader.onload = e => {
        text = e.target.result;
        data = csvToArray(text);
        tableText = buildTableFromData(data);
        // document.getElementById('text-output').value = JSON.stringify(table, null, 4);
        textOutput.value = tableText;
    }
    reader.readAsText(inputFile);
});

/**
 * Parse CSV file into JS Array of Objects
 * @param {string} str - CSV data
 * @param {string} delimiter
 * @returns Array
 */

function csvToArray(str, delimiter = ',') {
    // const headers = str.slice(0, str.indexOf("\n")).split(delimiter);
    // Need custom headers because merged cells don't export to csv properly
    const headers =
        ['Name', 'Rating', 'Trans',
            'SpeedMph', 'SpeedKph', 'SpeedStars',
            'Accel', 'AccelStars',
            'Weight', 'WeightStars',
            'Category', 'FolderName', 'Link', 'Status', 'Rotation'];
    // Category: Stock, DC, Custom
    // FolderName: used for sorting, image name
    // Link: RVW or whatever meme
    // Status: New, Updated, any string
    // Rotation: checkbox export, TRUE in, FALSE out

    const rows = str.slice(str.indexOf("\n") + 1).split("\n").filter(Boolean);
    const array = rows.map((row) => {
        const values = row.replace(/"+/g, '').split(delimiter);
        const element = headers.reduce((object, header, index) => {
            object[header] = values[index].trim();
            return object;
        }, {});
        return element;
    });
    return array;
}

/**
 * Builds Grav-shaped table from data.
 * Out-of-rotation cars placed at end
 * @param {array} data Parsed CSV data Object
 * @returns string
 */

function buildTableFromData(data) {

    let messageBuild = '';
    // messageBuild += 'Add any needed output like this!<br>';

    let outputsArray = [[], [], [], [], [], []];
    // 0 = Stock In
    // 1 = DC In
    // 2 = Custom In
    // 3 = Stock Out
    // 4 = DC Out
    // 5 = Custom Out

    data.forEach(car => {

        if (car.Rotation === "TRUE") {
            switch (car.Category) {
                case 'Stock':
                    outputsArray[0].push(car);
                    break;
                case 'DC':
                    outputsArray[1].push(car);
                    break;
                case 'Custom':
                    outputsArray[2].push(car);
                    break;
            }
        } else {
            switch (car.Category) {
                case 'Stock':
                    outputsArray[3].push(car);
                    break;
                case 'DC':
                    outputsArray[4].push(car);
                    break;
                case 'Custom':
                    outputsArray[5].push(car);
                    break;
            }
        }
    });

    outputsArray[0].sort((a, b) => stockCars.indexOf(a.FolderName) - stockCars.indexOf(b.FolderName));
    outputsArray[3].sort((a, b) => stockCars.indexOf(a.FolderName) - stockCars.indexOf(b.FolderName));

    outputsArray[1].sort((a, b) => dreamcastCars.indexOf(a.FolderName) - dreamcastCars.indexOf(b.FolderName));
    outputsArray[4].sort((a, b) => dreamcastCars.indexOf(a.FolderName) - dreamcastCars.indexOf(b.FolderName));

    outputsArray[2].sort((a, b) => a.Name.localeCompare(b.Name));
    outputsArray[5].sort((a, b) => a.Name.localeCompare(b.Name));

    // console.log('Array of arrays:', outputsArray);

    let stringBuild = '';
    let rotationStr = '';
    let newCarSmell = '';

    outputsArray.forEach((category, index) => {
        category.forEach(car => {

            if (car.Rotation === "TRUE") {
                rotationStr = `[![](${car.FolderName}.png?resize=170,170)](${car.Link})`
            } else {
                rotationStr = `<span style="filter: grayscale(100%) opacity(60%);"> [![](${car.FolderName}.png?resize=170,170)](${car.Link}) </span>\n\n![](outofrotation.png)`
            }

            if (car.Status) {
                newCarSmell = `
                    <p style="text-align:left; clear:both;">
                        <font size="4"><br/></font>
                        <span style="float:right;"><font size="4">
                            <b>${car.Status}</b>
                        </font></span>
                    </p>`.replace(/\ {20}/g, '            ');
            } else {
                newCarSmell = '';
            }

            nameSize = 4;
            if (car.Name.length >= 15) {
                nameSize = 3;
            }

            stringBuild +=
                `<p style="text-align:left;"><font size="${nameSize}"><b>
                    ${car.Name}
                </b></font>
                <span style="float:right;"><font size="3"><code>
                    ${car.Category}
                </code></font></span>
            </p>

            ${rotationStr}

            <p style="text-align:left; clear:both;">
                <font size="4"><b>
                    Top Speed:
                </b><br></font>
                <span style="float:left;"><font size="3">
                    ${car.SpeedMph}
                </font></span>
                <span style="float:right;"><font size="4"><code>
                    ${car.SpeedStars}
                </code></font></span>
            </p>
            <p style="text-align:left; clear:both;">
                <font size="4"><b>
                    Acceleration:
                </b><br></font>
                <span style="float:left;"><font size="3">
                    ${car.Accel}
                </font></span>
                <span style="float:right;"><font size="4"><code>
                    ${car.AccelStars}
                </code></font></span>
            </p>
            <p style="text-align:left; clear:both;">
                <font size="4"><b>
                    Weight:
                </b><br></font>
                <span style="float:left;"><font size="3">
                    ${car.Weight}
                </font></span>
                <span style="float:right;"><font size="4"><code>
                    ${car.WeightStars}
                </code></font></span>
            </p>${newCarSmell}`;

            stringBuild += '\n\n---\n\n'
        });

        if (index == 2) {
            // put thing here if you want it between in-rotation cars and out
            /*
            stringBuild += `
            ===[In-Rotation Cars End Here]===
            ---


            `;
            */
        }
    });

    stringBuild = stringBuild.replace(/\ {12}/gm, '');
    stringBuild = stringBuild.replace(/\n\n---\n\n$/, '');
    // remove excess indenting and lines at end

    if (messageBuild.length > 0) {
        document.getElementById('messages').innerHTML = messageBuild;
    }

    return stringBuild;
}

function copyOutputToClipboard() {
    navigator.clipboard.writeText(textOutput.value);
}
